<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('products');?>">Products</a>
			</li>
			<li class="breadcrumb-item active">Edit</li>
		</ol>
		<div class="row">
			<div class="col-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<fieldset>
						<legend style="font-size:18px;">Edit Product</legend>
						<form method="post" action="<?php echo base_url('products/editProduct/'.$product['p_public_id']);?>">
							<?php 
								$errors = $this->session->flashdata('errors');
								$values = $this->session->flashdata('values');
							?>
							<div class="form-group row">
								<label for="product-name" class="col-sm-3 col-form-label col-form-label-sm">Product's Name</label>
								<div class="col-sm-9">
									<input 
										type="text" 
										name="product-name" 
										value="<?php if (isset($values['product-name'])) {echo $values['product-name'];} else echo $product['p_name']; ?>" 
										class="form-control form-control-sm text-capitalize"
										id="product-name" required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['product-name'])) {echo $errors['product-name'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label col-form-label-sm" for="product-unit">Product's Unit</label>
								<div class="col-sm-9">
									<select name="product-unit" class="form-control form-control-sm" id="product-unit" required>
										<option value>None</option>
										<option value="Kg" selected="<?php if($product['p_unit'] == 'kg') echo 'selected'; ?>">Kilogram (Kg)</option>
										<option value="L" selected="<?php if($product['p_unit'] == 'l') echo 'selected'; ?>">Litre (L)</option>
									</select>
									<small class="form-text text-muted field-error"><?php if (isset($errors['product-unit'])) {echo $errors['product-unit'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label for="product-price" class="col-sm-3 col-form-label col-form-label-sm">Product's Price
									<br>( in <i class="fa fa-fw fa-inr"></i>)</label>
								<div class="col-sm-9">
									<input type="text" 
									name="product-price" 
									value="<?php if (isset($values['product-price'])) {echo $values['product-price'];} else echo $product['p_price']; ?>" 
									class="form-control form-control-sm text-capitalize" 
									id="product-price" required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['product-price'])) {echo $errors['product-price'];} ?></small>
								</div>
							</div>
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
							<button id="add" name="add-product" type="submit" class="btn btn-sm btn-success">Update</button>
						</form>
					</fieldset>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	  </div>
	</div>
</div>