<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('suppliers');?>">Suppliers</a>
			</li>
			<li class="breadcrumb-item active">Details</li>
		</ol>
		<div class="row">
			<div class="col-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<fieldset>
						<legend style="font-size:18px;">Supplier's Details</legend>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Name</label>
							<p class="col-sm-9 cst-detail"><?php echo ucwords($supplier['s_name']); ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Contact</label>
							<p class="col-sm-9 cst-detail"><?php echo $supplier['s_contact']; ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Address</label>
							<p class="col-sm-9 cst-detail"><?php echo $supplier['s_address']; ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Start Date</label>
							<p class="col-sm-9 cst-detail"><?php echo $supplier['s_start']; ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Product</label>
							<ol class="col-sm-9 cst-detail" style="list-style-type:none;">
								<?php 
									foreach ($products as $product) :
										if (in_array($product['p_public_id'], $supplier['s_product'])) :
								?>
											<li><i class="fa fa-fw fa-circle" style="font-size:8px;vertical-align:middle;"></i>  <?php echo ucwords($product['p_name'].' (in '.$product['p_unit'].')'); ?></li>
								<?php
										endif;
									endforeach;
								?>
							</ol>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Dues (in <i class="fa fa-fw fa-inr"></i>)</label>
							<p class="col-sm-9 cst-detail"><i class="fa fa-fw fa-inr"></i><?php echo $supplier['s_dues']; ?></p>
						</div>
					</fieldset>
					<br>
					<a href="<?php echo base_url('suppliers/edit/'.$supplier['s_public_id'])?>" class="btn btn-sm btn-warning">Edit Details</a>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	  </div>
	</div>
</div>