<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger'); ?>">Ledger</a>
			</li>
			<li class="breadcrumb-item active">Suppliers</li>
		</ol>
		<div class="col-md-12">
			
		</div>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-truck"></i> Suppliers List</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Contact</th>
							<th>Dues (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Contact</th>
							<th>Dues (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th></th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($suppliers != NULL):
							foreach ($suppliers as $supplier) :
						?>
							<tr>
								<td><?php echo ucwords($supplier['s_name']); ?></td>
								<td><?php echo $supplier['s_contact']; ?></td>
								<td><i class="fa fa-fw fa-inr"></i> <?php echo $supplier['s_dues']; ?></td>
								<td>
									<a href="<?php echo base_url('ledger/supplierLedger/'.$supplier['s_public_id'])?>" class="btn btn-sm btn-primary">View</a>
									<a href="<?php if(floatval($supplier['s_dues']) > 0) echo base_url('ledger/pay/'.$supplier['s_public_id']); else echo '#'?>" class="btn btn-sm btn-success">Pay</a>
								</td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>
</script>