<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('sell'); ?>">Sell Stock</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('sell/sell'); ?>">Sell</a>
			</li>
			<li class="breadcrumb-item active">Complete Sell</li>
		</ol>
		<div class="col-md-12 row">
			<div class="col-md-4">
				<input type="date" id="sell-date" class="form-control form-control-sm" value="<?php echo date('Y-m-d'); ?>" onchange="completeSell()">
			</div>
			<div class="col-md-4">
				<a href="<?php echo base_url('sell/completeSell'); ?>" id="complete-sell-btn" class="btn btn-sm btn-success">Complete</a>
			</div>
		</div>
		<hr>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-cubes"></i> Products List</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Price (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Instock</th>
							<th>In Cash</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Price (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Instock</th>
							<th>In Cash</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($products != NULL):
							foreach ($products as $product) :
								$incash = floatval($product['p_instock']) - floatval($product['p_daily']) + floatval($product['p_naga']);
								if(!($incash>0)) {
									$incash = 0;
								}
						?>
							<tr>
								<td><?php echo ucwords($product['p_name']); ?></td>
								<td><i class="fa fa-fw fa-inr"></i><?php echo $product['p_price']; ?></td>
								<td><?php echo $product['p_instock']; ?> <?php echo ucwords($product['p_unit']); ?></td>
								<td><?php echo ($incash.' '.ucwords($product['p_unit']) ); ?></td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>