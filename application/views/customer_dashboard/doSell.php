<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('sell'); ?>">Sell Stock</a>
			</li>
			<li class="breadcrumb-item active">Sell</li>
		</ol>
		<div class="col-md-12 row">
			<div class="col-md-4">
				<a href="#" class="btn btn-danger">Complete Sell</a>
			</div>
			<div class="col-md-2">
				<div class="input-group">
					<input type="date" id="date-filter" class="form-control form-control-sm" value="<?php echo date('Y-m-d'); ?>">
				</div>
			</div>
			<div class="col-md-2">
				<div class="input-group">
					<select class="form-control form-control-sm" id="pdt-filter">
						<?php 
						if( $products != NULL ):
							foreach ($products as $product) :
						?>
							<option value="<?php echo ucwords($product['p_public_id']); ?>"><?php echo ucwords($product['p_name']); ?></option>
						<?php 
							endforeach;
						endif;
						?>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<div class="input-group">
					<select class="form-control form-control-sm" id="time-filter">
						<option value="am">AM</option>
						<option value="pm">PM</option>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<div class="input-group">
					<a href="#" class="btn btn-sm btn-success" id="get-list-btn">Get List</a>
				</div>
			</div>
		</div>
		<hr>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-shopping-bag"></i> Sell</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="sellingTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Time</th>
							<th>Product</th>
							<th>Daily</th>
							<th></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Time</th>
							<th>Product</th>
							<th>Daily</th>
							<th></th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($customers != NULL):
							foreach ($customers as $customer) :
						?>
							<tr>
								<td><a href="<?php echo base_url('customers/details/'.$customer['c_public_id']); ?>" target="_blank" style="color: #222"><?php echo ucwords($customer['c_name']); ?></a></td>
								<td>
									<span><?php echo strtoupper($customer['c_daily']); ?></span>
								</td>
								<td>
									<span><?php echo ucwords($customer['p_name']); ?></span>
								</td>
								<td>
									<span><?php echo ucwords($customer['c_limit'].' '.$customer['p_unit']); ?></span>
								</td>
								<td>
									<button class="btn btn-sm btn-danger" data-cst="<?php echo $customer['c_public_id']; ?>" data-pdt="<?php echo $customer['c_product']; ?>" data-limit="<?php echo $customer['c_limit']; ?>" data-price="<?php echo $customer['p_price']; ?>" onclick="naga(this)">Naga</button>

									<a href="#" data-toggle="tooltip" data-placement="left" title="" class="error-xlc"><i class="fa fa-fw fa-exclamation-circle"></i></a>

									<button class="btn btn-sm btn-success" data-cst="<?php echo $customer['c_name']; ?>" data-id="<?php echo $customer['c_public_id']; ?>" data-pdt="<?php echo ucwords($customer['p_name']); ?>" data-limit="<?php echo ucwords($customer['c_limit'].' '.$customer['p_unit']); ?>" data-toggle="modal" data-target="#extraModalWindow" onclick="addCustomer(this)">Extra</button>
								</td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
		</div>
	</div>
</div>
<div class="modal fade" id="extraModalWindow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Add Extra</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reset()">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
			<div class="form-group col-sm-12 row">
				<label id="extra-customer-name-label" class="col-form-label col-sm-6">Customer:</label>
				<input type="text" readonly class="form-control-plaintext  form-control form-control-sm col-sm-6 text-capitalize" id="extra-customer-name" data-id="" value required disabled style="background-color: #fff;">
			</div>
			<div class="form-group col-sm-12 row">
				<label id="extra-customer-date-label" class="col-form-label col-sm-6">Date:</label>
				<input type="date" readonly class="form-control-plaintext form-control form-control-sm col-sm-6" id="extra-customer-date" value required disabled style="background-color: #fff;">
			</div>

			<div class="form-group col-sm-12 row">
				<label id="extra-customer-daily-pdt-label" class="col-form-label col-sm-6">Daily Product:</label>
				<input type="text" readonly class="form-control-plaintext form-control form-control-sm col-sm-6" id="extra-customer-daily-pdt" value required disabled style="background-color: #fff;">
			</div>
			<div class="form-group col-sm-12 row">
				<label id="extra-customer-limit-label" class="col-form-label col-sm-6">Daily Limit:</label>
				<input type="text" readonly class="form-control-plaintext form-control form-control-sm col-sm-6" id="extra-customer-limit" value required disabled style="background-color: #fff;">
			</div>
			<div class="form-group col-sm-12 row">
				<label id="extra-customer-pdt-label" class="col-form-label col-sm-6">Extra Product:</label>
				<select class="form-control form-control-sm col-sm-6" id="extra-customer-pdt" onchange="amount()" required>
					<?php 
					if( $products != NULL ):
						foreach ($products as $product) :
					?>
						<option value="<?php echo ucwords($product['p_public_id']); ?>" data-price="<?php echo $product['p_price']; ?>"><?php echo ucwords($product['p_name'].' (in '.$product['p_unit'].')'); ?></option>
					<?php 
						endforeach;
					endif;
					?>
				</select>
			</div>
			<div class="form-group col-sm-12 row">
				<label id="extra-customer-qty-label" class="col-form-label col-sm-6">Quantity:</label>
				<input type="text" class="form-control form-control-sm col-sm-6" id="extra-customer-qty" value  required onblur="checkQty(this)">
			</div>
			<div class="form-group col-sm-12 row">
				<label id="extra-customer-amt-label" class="col-form-label col-sm-6">Amount (in<i class="fa fa-fw fa-inr"></i>):</label>
				<input type="text" readonly class="form-control-sm form-control-plaintext col-sm-4" id="extra-customer-amt" value="0"  required>
			</div>
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" onclick="reset()">Cancel</button>
		<button type="button" class="btn btn-primary btn-sm" onclick="extra()">Add</button>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
	function changeGetURL() {
		var pdt = document.getElementById('pdt-filter').value;
		var date = document.getElementById('date-filter').value;
		var time = document.getElementById('time-filter').value;
		console.log(pdt,date,time);
		document.getElementById('get-list-btn').href = '<?php echo base_url('sell/sell/'); ?>'+pdt+'/'+date+'/'+time;
	}
</script>