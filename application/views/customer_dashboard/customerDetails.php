<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('customers');?>">Customers</a>
			</li>
			<li class="breadcrumb-item active">Details</li>
		</ol>
		<div class="row">
			<div class="col-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<fieldset>
						<legend style="font-size:18px;">Customer's Details</legend>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Name</label>
							<p class="col-sm-9 cst-detail"><?php echo ucwords($customer['c_name']); ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Contact</label>
							<p class="col-sm-9 cst-detail"><?php echo $customer['c_contact']; ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Address</label>
							<p class="col-sm-9 cst-detail"><?php echo $customer['c_address']; ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Start Date</label>
							<p class="col-sm-9 cst-detail"><?php echo $customer['c_start']; ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Times</label>
							<p class="col-sm-9 cst-detail"><?php echo strtoupper($customer['c_daily']); ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Product</label>
							<p class="col-sm-9 cst-detail"><?php echo ucwords($customer['p_name']); ?></p>
						</div>
						<div class="row">
							<label class="col-sm-3 col-form-label cst-det-label">Limit</label>
							<p class="col-sm-9 cst-detail"><?php echo $customer['c_limit'].' '.ucwords($customer['p_unit']); ?></p>
						</div>
					</fieldset>
					<br>
					<a href="<?php echo base_url('customers/edit/'.$customer['c_public_id'])?>" class="btn btn-sm btn-warning">Edit Details</a>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	  </div>
	</div>
</div>