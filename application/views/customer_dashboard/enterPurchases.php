<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('purchases'); ?>">Buy Stock</a>
			</li>
			<li class="breadcrumb-item active">Buy</li>
		</ol>
		<div class="col-md-12 row">
			<div class="col-md-9"></div>
			<div class="col-md-3">
				<div class="input-group">
					<input type="date" id="date-filter" class="form-control form-control-sm" value="<?php echo date('Y-m-d'); ?>">
					<span class="input-group-btn">
						<button class="btn btn-sm btn-primary" type="button"><i class="fa fa-fw fa-calendar"></i></button>
					</span>
				</div>
			</div>
		</div>
		<hr>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-book"></i> Buy</div>
		<div class="card-body">
			<div class="table-responsive text-center">
				<table class="table table-bordered" id="" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th rowspan="2" class="align-middle">Supplier's<br/>Name</th>
							<th rowspan="2" class="align-middle">Product</th>
							<th colspan="3">AM</th>
							<th colspan="3">PM</th>
							<th rowspan="2" class="align-middle">Total<br/>Amount</th>
							<th rowspan="2" class="align-middle">Paid</th>
							<th rowspan="2" class="align-middle">Buy</th>
						</tr>
						<tr>
							<th>Qty</th>
							<th>Amt</th>
							<th>Rate</th>
							<th>Qty</th>
							<th>Amt</th>
							<th>Rate</th>
						</tr>
					</thead>
					<tbody class="">
						<?php
							if($suppliers != NULL) :
								foreach ($suppliers as $supplier) :
						?>
						<tr>
							<td rowspan="<?php echo count($supplier['s_product'])+1; ?>" class="align-middle"><?php echo ucwords($supplier['s_name']); ?></td>
							<td colspan="7" style="padding:0;border:none;"></td>
							<td rowspan="<?php echo count($supplier['s_product'])+1; ?>" class="align-middle font-weight-bold"><span id="<?php echo $supplier['s_public_id'].'-t-amt'; ?>" data-amt>0</span></td>
							<td rowspan="<?php echo count($supplier['s_product'])+1; ?>" class="align-middle"><span><input type="text" class="prh-in" size="5" id="<?php echo $supplier['s_public_id'].'-paid'; ?>" onkeyup="checkPrice(this)"></span></td>
							<td rowspan="<?php echo count($supplier['s_product'])+1; ?>" class="align-middle"><button class="btn btn-sm btn-success" onclick='enterPurchase("<?php echo $supplier['s_public_id']; ?>", <?php echo json_encode($supplier["s_product"]); ?>)'>Buy</button></td>
						</tr>
						<?php
									foreach ($supplier['s_product'] as $product) :
										$product = $products[$product]
						?>
						<tr>
							<td><?php echo ucwords($product['p_name']); ?></td>
							<td class="table-warning"><span><input type="text" class="prh-in" size="5" id="<?php echo $supplier['s_public_id'].$product['p_public_id'].'-am-qty'; ?>" onkeyup="checkQty(this)" onblur='amount("<?php echo $supplier['s_public_id']; ?>", <?php echo json_encode($supplier["s_product"]); ?>)'></span></td>
							<td class="table-warning"><span><input type="text" class="prh-in" size="5" id="<?php echo $supplier['s_public_id'].$product['p_public_id'].'-am-amt'; ?>" onkeyup="checkPrice(this)" onblur='amount("<?php echo $supplier['s_public_id']; ?>", <?php echo json_encode($supplier["s_product"]); ?>)'></span></td>
							<td class="table-warning font-weight-bold"><span id="<?php echo $supplier['s_public_id'].$product['p_public_id'].'-am-rate'; ?>">0</span></td>
							<td class="table-danger"><span><input type="text" class="prh-in" size="5" id="<?php echo $supplier['s_public_id'].$product['p_public_id'].'-pm-qty'; ?>" onkeyup="checkQty(this)" onblur='amount("<?php echo $supplier['s_public_id']; ?>", <?php echo json_encode($supplier["s_product"]); ?>)'></span></td>
							<td class="table-danger"><span><input type="text" class="prh-in" size="5" id="<?php echo $supplier['s_public_id'].$product['p_public_id'].'-pm-amt'; ?>" onkeyup="checkPrice(this)" onblur='amount("<?php echo $supplier['s_public_id']; ?>", <?php echo json_encode($supplier["s_product"]); ?>)'></span></td>
							<td class="table-danger font-weight-bold"><span id="<?php echo $supplier['s_public_id'].$product['p_public_id'].'-pm-rate'; ?>">0</span></td>
						</tr>
						<?php
									endforeach;
								endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>