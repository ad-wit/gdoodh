<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger'); ?>">Ledger</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger/suppliers'); ?>">Suppliers</a>
			</li>
			<li class="breadcrumb-item active">View</li>
		</ol>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-truck"></i> Payments History</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="ledgerTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Bill Paid (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Bill Amount (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Balance (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Details</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Date</th>
							<th>Bill Paid (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Bill Amount (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Balance (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Details</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($ledger != NULL):
							foreach ($ledger as $entry) :
						?>
							<tr>
								<td><?php echo $entry['py_date']; ?></td>
								<td>
									<span><?php if($entry['py_credit']):?>
										<i class="fa fa-fw fa-inr"></i><?php echo $entry['py_amt']; ?>
									<?php else: ?>N/A<?php endif; ?></span>
								</td>
								<td>
									<span><?php if(!$entry['py_credit']):?>
										<i class="fa fa-fw fa-inr"></i><?php echo $entry['py_amt']; ?>
									<?php else: ?>N/A<?php endif; ?></span>
								</td>
								<td><i class="fa fa-fw fa-inr"></i><?php echo $entry['py_dues_left']; ?></td>
								<td>
									<?php if(!$entry['py_credit']):?>
										<button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#detailsModalWindow" onclick="viewBuy('<?php echo $entry["py_public_id"]; ?>')">Details</button>
										<a href="#" data-toggle="tooltip" data-placement="left" title="enter values" class="error-xlc"><i class="fa fa-fw fa-exclamation-circle"></i></a>
									<?php else: ?>N/A<?php endif; ?>
								</td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>
<div class="modal fade" id="detailsModalWindow" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Buy Details</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body text-capitalize">
			<div class="form-group col-sm-12 row">
				<span class="col-sm-6"><strong>Supplier</strong></span>
				<span class="col-sm-6" id="prhs-supplier"></span>
			</div>
			<div class="form-group col-sm-12 row">
				<span class="col-sm-6"><strong>Date</strong></span>
				<span class="col-sm-6" id="prhs-date"></span>
			</div>
			<div class="form-group col-sm-12 row">
				<span class="col-sm-6"><strong>Product</strong></span>
				<span class="col-sm-6" id="prhs-product"></span>
			</div>
			<div class="form-group col-sm-12 row">
				<span class="col-sm-6"><strong>Quantity</strong></span>
				<span class="col-sm-6" id="prhs-qty"></span>
			</div>
			<div class="form-group col-sm-12 row">
				<span class="col-sm-6"><strong>Rate (<i class="fa fa-fw fa-inr"></i>)</strong></span>
				<span class="col-sm-6" id="prhs-rate"></span>
			</div>
			<div class="form-group col-sm-12 row">
				<span class="col-sm-6"><strong>Amount (<i class="fa fa-fw fa-inr"></i>)</strong></span>
				<span class="col-sm-6" id="prhs-amt"></span>
			</div>
		</div>
	</div>
	</div>
</div>