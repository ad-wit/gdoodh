<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('customers');?>">Customers</a>
			</li>
			<li class="breadcrumb-item active">Edit</li>
		</ol>
		<div class="row">
			<div class="col-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<fieldset>
						<legend style="font-size:18px;">Edit Customer</legend>
						<form method="post" action="<?php echo base_url('customers/editCustomer/'.$customer['c_public_id']);?>" style="margin-bottom:50px;">
							<?php 
								$errors = $this->session->flashdata('errors');
								$values = $this->session->flashdata('values');
							?>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-name">Customer's Name</label>
								<div class="col-sm-8">
									<input 
										type="text" 
										name="customer-name" 
										value="<?php if (isset($values['customer-name'])) {echo $values['customer-name'];} else echo $customer['c_name']; ?>" 
										class="form-control form-control-sm" 
										id="customer-name"
										maxlength=50 required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['customer-name'])) {echo $errors['customer-name'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-contact">Customer's Contact</label>
								<div class="col-sm-8">
									<input 
										type="text" 
										name="customer-contact" 
										value="<?php if (isset($values['customer-contact'])) {echo $values['customer-contact'];} else echo $customer['c_contact']; ?>" 
										class="form-control form-control-sm" 
										id="customer-contact"
										maxlength=13 required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['customer-contact'])) {echo $errors['customer-contact'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-address">Customer's Address</label>
								<div class="col-sm-8">
									<textarea
										name="customer-address" 
										class="form-control form-control-sm" 
										id="customer-address"
										maxlength=100><?php if (isset($values['customer-address'])) {echo $values['customer-address'];} else echo $customer['c_address']; ?></textarea>
									<small class="form-text text-muted field-error"><?php if (isset($errors['customer-address'])) {echo $errors['customer-address'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-start">Customer's Start Date</label>
								<div class="col-sm-8">
									<input 
										type="date" 
										name="customer-start" 
										value="<?php if (isset($values['customer-start'])) {echo $values['customer-start'];} else echo $customer['c_start']; ?>" 
										class="form-control form-control-sm" 
										id="customer-start" required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['customer-start'])) {echo $errors['customer-start'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-time">Customer's Time</label>
								<div class="col-sm-8">
									<select 
										name="customer-time" 
										class="form-control form-control-sm" 
										id="customer-time" required
										>
										<option value="am" <?php if($customer['c_daily'] == 'am'){echo 'selected';} ?>>AM</option>
										<option value="pm" <?php if($customer['c_daily'] == 'pm'){echo 'selected';} ?>>PM</option>
									</select>
									<small class="form-text text-muted field-error"><?php if (isset($errors['customer-time'])) {echo $errors['customer-time'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-product">Customer's Products</label>
								<div class="col-sm-8">
									<div class="form-check">
										<select 
											name="customer-product" 
											class="form-control form-control-sm" 
											id="customer-product" 
											required
											>
											<option value="">None</option>
											<?php foreach ($products as $product): ?>
												<option value="<?php echo $product['p_public_id']; ?>" 
													<?php if($customer['c_product'] === $product['p_public_id']) { echo 'selected'; } ?>><?php echo ucwords($product['p_name'].' (in '.$product['p_unit'].')'); ?></option>
											<?php endforeach; ?>
										</select>
										<small class="form-text text-muted field-error"><?php if (isset($errors['customer-product'])) {echo $errors['customer-product'];} ?></small>
									</div>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="customer-limit">Customer's Daily Limit</label>
								<div class="col-sm-8">
									<input 
										type="text" 
										name="customer-limit" 
										value="<?php if (isset($values['customer-limit'])) {echo $values['customer-limit'];} else echo $customer['c_limit']; ?>" 
										class="form-control form-control-sm" 
										id="customer-limit" 
										maxlength=10 required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['customer-limit'])) {echo $errors['customer-limit'];} ?></small>
								</div>
							</div>
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
							<button id="add" type="submit" class="btn btn-sm btn-success">Update</button>
						</form>
					</fieldset>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	  </div>
	</div>
</div>
