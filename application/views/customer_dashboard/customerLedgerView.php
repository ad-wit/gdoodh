<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$customer = $ledger['customer'];
	$sell = $ledger['sell'];
	// $totOfMonth = cal_days_in_month(CAL_GREGORIAN, $month);
	// echo strtotime();
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger'); ?>">Ledger</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger/customers'); ?>">Customers</a>
			</li>
			<li class="breadcrumb-item active">View</li>
		</ol>
		<div class="col-md-12 row">
			<div class="col-md-3">
				<div class="input-group">
					<input type="month" id="bill-month" class="form-control form-control-sm" value="<?php if($month == NULL) echo date('Y-m'); else echo $month ?>" data-cid="<?php echo $customer['c_public_id']; ?>" onchange="setDate()">
					<span class="input-group-btn">
						<a href="<?php echo base_url('ledger/customerLedger/'.$customer['c_public_id']); ?>" id="get-bill" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-arrow-right"></i></a>
					</span>
				</div> 
			</div>
		</div>
		<hr>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-user"></i> <?php echo ucwords($customer['c_name']); ?></div>
		<div class="card-body">
			<h5 class="text-center">Monthly Entry</h5>
			<div class="table-responsive">
				<table class="table table-bordered table" id="" width="100%" cellspacing="0">
					<thead>
						<tr class="table-primary">
							<th>Date</th>
							<th>Naga</th>
							<th>Product</th>
							<th>Qty</th>
							<th>Price (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Amount (in <i class="fa fa-fw fa-inr"></i>)</th>
						</tr>
					</thead>
					<tfoot>
					</tfoot>
					<tbody>
						<?php
							if($sell != NULL):
								$totExAmt = 0;
								$naga = 0;
								foreach ($sell as $entry) :
									if($entry['sl_is_naga']) :
										$naga++;
						?>
										<tr class="table-danger">
											<td><?php echo $entry['sl_date']; ?></td>
											<td><i class="fa fa-fw fa-times" style="color:#dc3545;"></td>
											<td><?php echo ucwords($entry['p_name']); ?></td>
											<td><?php echo $entry['sl_qty']; ?> <?php echo ucwords($entry['p_unit']); ?></td>
											<td>N/A</td>
											<td>N/A</td>
										</tr>
						<?php
									else :
										$amt = floatval($entry['sl_qty']) * floatval($entry['sl_price']);
										$totExAmt = $totExAmt + $amt;
						?>
										<tr class="table-success">
											<td><?php echo $entry['sl_date']; ?></td>
											<td><i class="fa fa-fw fa-check" style="color:#28a745;"></td>
											<td><?php echo ucwords($entry['p_name']); ?></td>
											<td><?php echo $entry['sl_qty']; ?> <?php echo ucwords($entry['p_unit']); ?></td>
											<td><i class="fa fa-fw fa-inr"></i><?php echo $entry['sl_price']; ?></td>
											<td><i class="fa fa-fw fa-inr"></i><?php echo $amt; ?></td>
										</tr>
						<?php
									endif;
								endforeach;
						?>
								<tr class="font-weight-bold">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="table-warning">Total Naga</td>
									<td class="table-warning"><?php echo $naga; ?></td>
								</tr>
								<tr class="font-weight-bold">
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td class="table-warning">Total Extra(in <i class="fa fa-fw fa-inr"></i>)</td>
									<td class="table-warning"><i class="fa fa-fw fa-inr"></i><?php echo $totExAmt; ?></td>
								</tr>
						<?php
							else :
						?>
								<tr class="text-center">
									<td colspan="6">No Entry</td>
								</tr>
						<?php
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>