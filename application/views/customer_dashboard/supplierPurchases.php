<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger'); ?>">Ledger</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger/suppliers'); ?>">Suppliers</a>
			</li>
			<li class="breadcrumb-item active">View</li>
		</ol>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-cubes"></i> Purchase History</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Date</th>
							<th>Product</th>
							<th>Rate (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Quantity</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Date</th>
							<th>Product</th>
							<th>Rate (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Quantity</th>
							<th>Amount</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($purchases != NULL):
							foreach ($purchases as $purchase) :
						?>
							<tr>
								<td><?php echo $purchase['prh_date']; ?></td>
								<td><?php echo ucwords($purchase['p_name']); ?></td>
								<td><i class="fa fa-fw fa-inr"></i><?php echo $purchase['prh_rate']; ?></td>
								<td><?php echo $purchase['prh_qty']; ?> <?php echo ucwords($purchase['p_unit']); ?></td>
								<td><i class="fa fa-fw fa-inr"></i><?php echo $purchase['prh_amt']; ?></td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>