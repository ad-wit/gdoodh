<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Ledger</li>
		</ol>
		<div class="row">
			<div class="col-xl-3 col-sm-6 mb-3">
				<div class="card text-white bg-primary o-hidden h-100">
					<div class="card-body">
						<div class="card-body-icon">
							<i class="fa fa-fw fa-truck"></i>
						</div>
						<div class="mr-5">Suppliers Ledger</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('ledger/suppliers') ?>">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fa fa-angle-right"></i>
						</span>
					</a>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6 mb-3">
				<div class="card text-white bg-success o-hidden h-100">
					<div class="card-body">
						<div class="card-body-icon">
							<i class="fa fa-fw fa-users"></i>
						</div>
						<div class="mr-5">Customers Ledger</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('ledger/customers') ?>">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fa fa-angle-right"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
	  </div>
	</div>
</div>