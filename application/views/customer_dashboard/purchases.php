<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Buy Stock</li>
		</ol>
		<div class="col-md-12 row">
			<div class="col-md-3">
				<a href="<?php echo base_url('purchases/purchase'); ?>" class="btn btn-success">Buy</a>
			</div>
			<div class="col-md-6">
			</div>
			<div class="col-md-3">
				<div class="input-group">
					<input type="date" id="date-filter" class="form-control form-control-sm" value="<?php if(isset($date)) echo $date; else echo date('Y-m-d'); ?>" onchange="setDate()">
					<span class="input-group-btn">
						<a href="<?php echo base_url('purchases'); ?>" class="btn btn-sm btn-primary" id="get-stock-btn"><i class="fa fa-fw fa-arrow-right"></i></a>
					</span>
				</div>
			</div>
		</div>
		<hr>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-cubes"></i> Products List</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Price (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Instock</th>
							<th>In Cash</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Price (in <i class="fa fa-fw fa-inr"></i>)</th>
							<th>Instock</th>
							<th>In Cash</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($products != NULL):
							foreach ($products as $product) :
						?>
							<tr>
								<td><?php echo ucwords($product['p_name']); ?></td>
								<td><i class="fa fa-fw fa-inr"></i><?php echo $product['p_price']; ?></td>
								<td><?php echo $product['stock']; ?> <?php echo ucwords($product['p_unit']); ?></td>
								<td>
									
								</td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>
<script type="text/javascript">
	function setDate() {
		document.getElementById('get-stock-btn').setAttribute('href', '<?php echo base_url("purchases/index/"); ?>'+document.getElementById('date-filter').value);
	}
</script>
