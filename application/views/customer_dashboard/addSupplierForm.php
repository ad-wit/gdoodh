<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('suppliers');?>">Suppliers</a>
			</li>
			<li class="breadcrumb-item active">Add</li>
		</ol>
		<div class="row">
			<div class="col-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<fieldset>
						<legend style="font-size:18px;">Add Supplier</legend>
						<form method="post" action="<?php echo base_url('suppliers/addSupplier');?>" style="margin-bottom:50px;">
							<?php 
								$errors = $this->session->flashdata('errors');
								$values = $this->session->flashdata('values');
							?>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="supplier-name">Supplier's Name</label>
								<div class="col-sm-8">
									<input 
										type="text" 
										name="supplier-name" 
										value="<?php if (isset($values['supplier-name'])) {echo $values['supplier-name'];} ?>" 
										class="form-control form-control-sm" 
										id="supplier-name"
										maxlength=50 required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['supplier-name'])) {echo $errors['supplier-name'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="supplier-contact">Supplier's Contact</label>
								<div class="col-sm-8">
									<input 
										type="text" 
										name="supplier-contact" 
										value="<?php if (isset($values['supplier-contact'])) {echo $values['supplier-contact'];} ?>" 
										class="form-control form-control-sm" 
										id="supplier-contact"
										maxlength=13 required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['supplier-contact'])) {echo $errors['supplier-contact'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="supplier-address">Supplier's Address</label>
								<div class="col-sm-8">
									<textarea
										name="supplier-address" 
										class="form-control form-control-sm" 
										id="supplier-address"
										maxlength=100><?php if (isset($values['supplier-address'])) {echo $values['supplier-address'];} ?></textarea>
									<small class="form-text text-muted field-error"><?php if (isset($errors['supplier-address'])) {echo $errors['supplier-address'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="supplier-start">Supplier's Start Date</label>
								<div class="col-sm-8">
									<input 
										type="date" 
										name="supplier-start" 
										data-toggle="datepicker" 
										value="<?php if (isset($values['supplier-start'])) {echo $values['supplier-start'];} ?>" 
										class="form-control form-control-sm" 
										id="supplier-start" required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['supplier-start'])) {echo $errors['supplier-start'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="supplier-product">Supplier's Products</label>
								<div class="col-sm-8">
									<div class="form-check">
										<?php foreach ($products as $product): ?>
											<div class="form-check form-check-inline">
												<input class="" type="checkbox" id="supplier-product" value="<?php echo $product['p_public_id']; ?>" name="supplier-product[]" <?php  if (isset($values['supplier-product'])) if(in_array($product['p_public_id'], $values['supplier-product'])) echo "checked"; ?>>
												<label class="" for="supplier-product"><?php echo ucwords($product['p_name'].' (in '.$product['p_unit'].')'); ?></label>
											</div>
										<?php endforeach; ?>
									</div>
									<small class="form-text text-muted field-error"><?php if (isset($errors['supplier-product'])) {echo $errors['supplier-product'];} ?></small>
								</div>
							</div>
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
							<button id="add" type="submit" class="btn btn-sm btn-success">Add</button>
						</form>
					</fieldset>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	  </div>
	</div>
</div>
