<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url();?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger');?>">Ledger</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger/suppliers');?>">Suppliers</a>
			</li>
			<li class="breadcrumb-item active">Pay</li>
		</ol>
		<div class="row">
			<div class="col-12">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<fieldset>
						<legend style="font-size:18px;">Pay To Supplier</legend>
						<form method="post" action="<?php echo base_url('ledger/makePayment/'.$supplier['s_public_id']);?>" style="margin-bottom:50px;">
							<?php 
								$errors = $this->session->flashdata('errors');
								$values = $this->session->flashdata('values');
							?>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm">Name</label>
								<div class="col-sm-8">
									<input 
										type="text" 
										readonly 
										value="<?php echo ucwords($supplier['s_name']); ?>" 
										class="form-control-plaintext form-control-sm" 
										maxlength=50 required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm">Contact</label>
								<div class="col-sm-8">
									<input 
										type="text"  
										readonly 
										value="<?php echo $supplier['s_contact']; ?>" 
										class="form-control-plaintext form-control-sm" 
										maxlength=13 required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm">Dues</label>
								<div class="col-sm-8">
									<span><i class="fa fa-fw fa-inr"></i></span>
									<input 
										type="text" 
										name="payment-dues" 
										readonly 
										value="<?php echo $supplier['s_dues']; ?>" 
										class="form-control-plaintext form-control-sm" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="payment-amt">Payment Amount</label>
								<div class="col-sm-8">
									<span><i class="fa fa-fw fa-inr"></i></span>
									<input 
										type="text" 
										name="payment-amt" 
										value="<?php if (isset($values['payment-amt'])) {echo $values['payment-amt'];}?>" 
										class="form-control-sm" 
										onblur="validPrice(this)" 
										id="payment-amt" required>
									<a href="#" data-toggle="tooltip" data-placement="right" title="Invalid Amount" class="error-xlc"><i class="fa fa-fw fa-exclamation-circle"></i></a>
									<small class="form-text text-muted field-error"><?php if (isset($errors['payment-amt'])) {echo $errors['payment-amt'];} ?></small>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm">Dues Left</label>
								<div class="col-sm-8">
									<span><i class="fa fa-fw fa-inr"></i></span>
									<input 
										type="text" 
										id="dues-left"  
										readonly 
										value="<?php echo $supplier['s_dues']; ?>" 
										class="form-control-plaintext form-control-sm" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm" for="payment-date">Payment Date</label>
								<div class="col-sm-5">
									<input 
										type="date" 
										name="payment-date" 
										value="<?php if (isset($values['payment-date'])) {echo $values['payment-date'];} else echo date('Y-m-d');?>" 
										class="form-control form-control-sm" 
										id="payment-date" required>
									<small class="form-text text-muted field-error"><?php if (isset($errors['payment-date'])) {echo $errors['payment-date'];} ?></small>
								</div>
								<div class="col-sm-3"></div>
							</div>
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
							<button id="add" type="submit" class="btn btn-sm btn-success">Pay</button>
						</form>
					</fieldset>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	  </div>
	</div>
</div>
<script type="text/javascript">
	function validPrice(el) {
		var pat = /^\d{0,8}(\.\d{1,2})?$/;
		var dues = Number("<?php echo $supplier['s_dues']; ?>");
		if (!pat.test(el.value)) {
			el.nextElementSibling.style.display = 'inline';
			document.getElementById('dues-left').value = dues;
		} else {
			el.nextElementSibling.style.display = 'none';
			var amt = Number(document.getElementById('payment-amt').value);
			var left = dues - amt;
			if (left < 0) {
				el.nextElementSibling.style.display = 'inline';
				document.getElementById('dues-left').value = dues;
				document.getElementById('payment-amt').value = "";
			} else {
				document.getElementById('dues-left').value = left;
			}
		}
	}
</script>