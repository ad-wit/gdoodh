<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="<?php echo base_url(); ?>">Dashboard</a>
			</li>
			<li class="breadcrumb-item">
				<a href="<?php echo base_url('ledger'); ?>">Ledger</a>
			</li>
			<li class="breadcrumb-item active">Customers</li>
		</ol>
		<div class="col-md-12">
			
		</div>
		<div class="card mb-3">
		<div class="card-header"><i class="fa fa-fw fa-users"></i> Customers List</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Contact</th>
							<th>Start</th>
							<th>Details</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Contact</th>
							<th>Start</th>
							<th>Details</th>
						</tr>
					</tfoot>
					<tbody>
						<?php
							if($customers != NULL):
							foreach ($customers as $customer) :
						?>
							<tr>
								<td><?php echo ucwords($customer['c_name']); ?></td>
								<td><?php echo $customer['c_contact']; ?></td>
								<td><?php echo $customer['c_start']; ?></td>
								<td>
									<a href="<?php echo base_url('ledger/customerLedger/'.$customer['c_public_id'])?>" class="btn btn-sm btn-primary">View</a>
								</td>
							</tr>
						<?php
							endforeach;
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div>
	  </div>
	</div>
</div>