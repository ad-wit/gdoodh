<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Gdoodh Service</title>
	<!-- Bootstrap core CSS-->
	<link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<!-- Datepicker CSS-->
	<link href="<?php echo base_url('assets/vendor/datepicker/datepicker.min.css'); ?>" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="<?php echo base_url('assets/vendor/datatables/dataTables.bootstrap4.css'); ?>" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="<?php echo base_url('assets/css/sb-admin.css'); ?>" rel="stylesheet">
	<!-- Custom styles by developer-->
	<link href="<?php echo base_url('assets/css/main.css'); ?>" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<div class="alert-msg" style="position:absolute;width:100%;z-index:100">
		<?php if($this->session->flashdata('success')): ?>
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('error')): ?>
			<div class="alert alert-danger alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php endif; ?>
	</div>
