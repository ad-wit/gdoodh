<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">

	function  completeSell() {
		$('#complete-sell-btn').attr( 'href', '<?php echo base_url('sell/completeSell'); ?>/'+$('#sell-date').val() );
	}
	<?php if( $this->session->flashdata('error') != NULL ) : ?>

		$.notify({
			message: "Error! <?php echo $this->session->flashdata('error') ?>" 
		},{
			type: 'danger',
			delay: -1500
		});

	<?php elseif( $this->session->flashdata('success') != NULL ) : ?>

		$.notify({
			message: "Success! <?php echo $this->session->flashdata('success') ?>" 
		},{
			type: 'success',
			delay: -1500
		});

	<?php endif; ?>
</script>