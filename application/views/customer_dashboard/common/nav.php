<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<a class="navbar-brand" href="<?php echo base_url();?>">Gdoodh</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
					<a class="nav-link" href="<?php echo base_url();?>">
						<i class="fa fa-fw fa-dashboard"></i>
						<span class="nav-link-text">Dashboard</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Purchases">
					<a class="nav-link" href="<?php echo base_url('purchases');?>">
						<i class="fa fa-fw fa-book"></i>
						<span class="nav-link-text">Buy</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sell">
					<a class="nav-link" href="<?php echo base_url('sell');?>">
						<i class="fa fa-fw fa-shopping-bag"></i>
						<span class="nav-link-text">Sell</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Products">
					<a class="nav-link" href="<?php echo base_url('products');?>">
						<i class="fa fa-fw fa-cubes"></i>
						<span class="nav-link-text">Products</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Customers">
					<a class="nav-link" href="<?php echo base_url('customers');?>">
						<i class="fa fa-fw fa-users"></i>
						<span class="nav-link-text">Customers</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Suppliers">
					<a class="nav-link" href="<?php echo base_url('suppliers');?>">
						<i class="fa fa-fw fa-truck"></i>
						<span class="nav-link-text">Suppliers</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Delivery">
					<a class="nav-link" href="#">
						<i class="fa fa-fw fa-motorcycle"></i>
						<span class="nav-link-text">Delivery</span>
					</a>
				</li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Ledger">
					<a class="nav-link" href="<?php echo base_url('ledger');?>">
						<i class="fa fa-pencil-square-o"></i>
						<span class="nav-link-text">Ledger</span>
					</a>
				</li>
			</ul>
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item">
					<a class="nav-link text-center" id="sidenavToggler">
					<i class="fa fa-fw fa-angle-left"></i>
					</a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto">
				
				<li class="nav-item">
					<!-- <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
					<i class="fa fa-fw fa-sign-out"></i>Logout</a> -->
				</li>
			</ul>
		</div>
	</nav>
