<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">

	function checkPrice(el) {
		var pat = /^\d{0,8}(\.\d{1,2})?$/;
		if (!pat.test(el.value)) {
			$(el).addClass('wrong-in');
		} else {
			$(el).removeClass('wrong-in');
		}
	}

	function checkQty(el) {
		var pat = /^\d{0,10}(\.\d{1,4})?$/;
		if (!pat.test(el.value)) {
			$(el).addClass('wrong-in');
		} else {
			$(el).removeClass('wrong-in');
		}
	}

	function amount(supplier, products) {

		var tAmt = 0;

		for (var key in products) {

			var product = products[key];

			var am_qty = Number(document.getElementById(supplier+product+'-am-qty').value);
			var am_amt = Number(document.getElementById(supplier+product+'-am-amt').value);

			if (!isNaN(am_qty) && !isNaN(am_amt) &&  am_qty!= 0 && am_amt!= 0) {

				var rate = am_amt/am_qty;
				document.getElementById(supplier+product+'-am-rate').innerHTML = rate;
				tAmt = tAmt + am_amt;
			}
			
			var pm_qty = Number(document.getElementById(supplier+product+'-pm-qty').value);
			var pm_amt = Number(document.getElementById(supplier+product+'-pm-amt').value);

			if (!isNaN(pm_qty) && !isNaN(pm_amt) && pm_qty != 0 && pm_amt!= 0) {

				var rate = pm_amt/pm_qty;
				document.getElementById(supplier+product+'-pm-rate').innerHTML = rate;
				tAmt = tAmt + pm_amt;
			}
		}

		document.getElementById(supplier+'-t-amt').innerHTML = tAmt;		
	}

	function enterPurchase(supplier, products) {

		var totalAmount = 0;

		for (var key in products) {

			var product = products[key];

			var am_qty_el = document.getElementById(supplier+product+'-am-qty');
			var am_amt_el = document.getElementById(supplier+product+'-am-amt');
			var am_qty = Number(am_qty_el.value);
			var am_amt = Number(am_amt_el.value);

			if (!isNaN(am_qty) && !isNaN(am_amt) &&  am_qty!= 0 && am_amt!= 0 && !am_qty_el.getAttribute('disabled') && !am_amt_el.getAttribute('disabled')) {

				var rate = am_amt/am_qty;
				var purchase = {
					'p_supplier': supplier,
					'p_product': product,
					'p_date': document.getElementById('date-filter').value,
					'p_time': 'am',
					'p_rate': rate,
					'p_qty': am_qty,
					'p_amt': am_amt,
					'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
				}
				totalAmount = totalAmount + purchase['p_amt'];
				$.ajax({
					url : '<?php echo base_url("purchases/enterPurchase");?>',
					type : 'POST',
					data : purchase,
					success :   function(data, status) {
									// console.log('prh am -');
									// console.log(data);
									document.getElementById(supplier+product+'-am-qty').setAttribute('disabled', true);
									document.getElementById(supplier+product+'-am-amt').setAttribute('disabled', true);
								}
				});
			}
			
			var pm_qty_el = document.getElementById(supplier+product+'-pm-qty');
			var pm_amt_el = document.getElementById(supplier+product+'-pm-amt');
			var pm_qty = Number(pm_qty_el.value);
			var pm_amt = Number(pm_amt_el.value);

			if (!isNaN(pm_qty) && !isNaN(pm_amt) &&  pm_qty!= 0 && pm_amt!= 0 && !pm_qty_el.getAttribute('disabled') && !pm_amt_el.getAttribute('disabled')) {

				var rate = pm_amt/pm_qty;
				var purchase = {
					'p_supplier': supplier,
					'p_product': product,
					'p_date': document.getElementById('date-filter').value,
					'p_time': 'pm',
					'p_rate': rate,
					'p_qty': pm_qty,
					'p_amt': pm_amt,
					'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
				}
				totalAmount = totalAmount + purchase['p_amt'];
				$.ajax({
					url : '<?php echo base_url("purchases/enterPurchase");?>',
					type : 'POST',
					data : purchase,
					success :   function(data, status) {
									// console.log('prh pm -');
									// console.log(data);
									pm_qty_el.setAttribute('disabled', true);
									pm_amt_el.setAttribute('disabled', true);
								}
				});
			}
		}

		if (totalAmount) {

			$.ajax({
				url : '<?php echo base_url("purchases/addAmount");?>',
				type : 'POST',
				data : {
					'a_supplier': supplier,
					'a_date': document.getElementById('date-filter').value,
					'a_amount' : totalAmount,
					'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
				},
				success :   function(data, status) {
								// console.log('total amount =');
								// console.log(data);
								var paidAmount = Number(document.getElementById(supplier+'-paid').value);
								if ( !isNaN(paidAmount) && paidAmount <= totalAmount ) {

									$.ajax({
										url : '<?php echo base_url("purchases/paidAmount");?>',
										type : 'POST',
										data : {
												'pd_supplier': supplier,
												'pd_date': document.getElementById('date-filter').value,
												'pd_amount' : paidAmount,
												'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
										},
										success :   function(data, status) {
														// console.log('paid amount =');
														// console.log(data);
													}
									});
								}
							}
			});
		}

	}

</script>