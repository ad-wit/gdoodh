<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">

	function viewBuy(id) {

		$.get('<?php echo base_url("purchases/getPurchase/") ?>'+id, function (data, status) {
			
			data = JSON.parse(data);
			if (data.success) {
				data = data.success;
				$('#prhs-supplier').html(data.s_name);
				$('#prhs-date').html(data.prh_date);
				$('#prhs-product').html(data.p_name);
				$('#prhs-qty').html(data.prh_qty+' '+data.p_unit);
				$('#prhs-rate').html('<i class="fa fa-fw fa-inr"></i>'+data.prh_rate);
				$('#prhs-amt').html('<i class="fa fa-fw fa-inr"></i>'+data.prh_amt);
			} else {

				$('#prhs-supplier').html('N/A');
				$('#prhs-date').html('N/A');
				$('#prhs-product').html('N/A');
				$('#prhs-qty').html('N/A');
				$('#prhs-rate').html('N/A');
				$('#prhs-amt').html('N/A');
			}
		});
	}

	$(document).ready(function() {
		var table = $('#ledgerTable').DataTable({
			"pageLength": 100
		});
	} );
</script>