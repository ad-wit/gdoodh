<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">

	function checkQty(el, id) {
		var pat = /^\d{0,10}(\.\d{1,4})?$/;
		if (!pat.test(el.value)) {
			$('#extra-customer-qty').addClass('wrong-in');
		} else {
			$('#extra-customer-qty').removeClass('wrong-in');
			amount();
		}
	}

	function amount() {
		var price = Number($($('select#extra-customer-pdt option:selected')[0]).data('price'));
		var qty = Number($('#extra-customer-qty').val());
		var amt = price * qty;
		if (Number(amt)) {
			$('#extra-customer-amt').val(amt);
		} else {
			$('#extra-customer-amt').val('0');
		}
	}

	function addCustomer(el) {
		$('#extra-customer-name').val($(el).data('cst'));
		$('#extra-customer-name').data('id', $(el).data('id'));
		$('#extra-customer-date').val($('#date-filter').val());
		$('#extra-customer-daily-pdt').val($(el).data('pdt'));
		$('#extra-customer-limit').val($(el).data('limit'));
	}

	function reset() {
		$('#extra-customer-name').data('id', '');
		$('#extra-customer-name').val('');
		$('#extra-customer-daily-pdt').val('');
		$('#extra-customer-limit').val('');
		$('#extra-customer-qty').val('');
		$('#extra-customer-amt').val('0');
	}

	function extra() {
		$('#extra-customer-date').val($('#date-filter').val());
		amt = $('#extra-customer-amt').val();
		if( amt != '0' ) {
			
			$.ajax({
				url : '<?php echo base_url("sell/extra");?>',
				type : 'POST',
				data : {
							'x_customer': $('#extra-customer-name').data('id'),
							'x_pdt': $('#extra-customer-pdt').val(),
							'x_date': $('#date-filter').val(),
							'x_qty': $('#extra-customer-qty').val(),
							'x_price': $($('select#extra-customer-pdt option:selected')[0]).data('price'),
							'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
						},
				success :   function(data, status) {

								data = JSON.parse(data);
								$('#extraModalWindow').modal('hide');
								reset();

								if (data.success) {

									$.notify({
										message: 'Extra product added.' 
									},{
										type: 'success',
										delay: 1500
									});
								} else {
									$.notify({
										message: data.error 
									},{
										type: 'danger',
										delay: 2000
									});
								}
							},
				error   :   function(xhr) {
								el.nextElementSibling.setAttribute('title', 'server error');
								el.nextElementSibling.style.display = 'inline';
							}
			});
		}
	}

	function naga(el) {
		
		$.ajax({
			url : '<?php echo base_url("sell/naga");?>',
			type : 'POST',
			data : {
						'n_customer': $(el).data('cst'),
						'n_product': $(el).data('pdt'),
						'n_limit': $(el).data('limit'),
						'n_price': $(el).data('price'),
						'n_date': $("#date-filter").val(),
						'<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
					},
			success :   function(data, status) {

							data = JSON.parse(data);
							if (data.success) {
								el.setAttribute('disabled', true);
								$(el).removeClass('btn-danger');
								$(el).addClass('btn-secondary');
								el.nextElementSibling.setAttribute('title', '');
								el.nextElementSibling.style.display = 'none';
								el.nextElementSibling.nextElementSibling.setAttribute('disabled', true);
							} else {
								el.nextElementSibling.setAttribute('title', data.error);
								el.nextElementSibling.style.display = 'inline';
							}
						},
			error   :   function(xhr) {
							el.nextElementSibling.setAttribute('title', 'server error');
							el.nextElementSibling.style.display = 'inline';
						}
		});
	}
	 
	$(document).ready(function() {
		var table = $('#sellingTable').DataTable({
			"pageLength": 100
		});

		$('#pdt-filter-btn').on( 'click', function() {
			if ($('#pdt-filter').val()) {
				table.columns(2).search( $('#pdt-filter').val() ).draw();
			} else{
				table.columns('').search('').draw();
			} 
		});

		$('#time-filter-btn').on( 'click', function() {
			if ($('#time-filter').val()) {
				table.columns(1).search( $('#time-filter').val() ).draw();
			} else{
				table.columns('').search('').draw();
			} 
		});
	} );

</script>