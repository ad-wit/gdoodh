<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sell extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model('Sell_Model');
	}

	public function index($date = NULL) {

		$data = $this->Common_Model->getProductsStock($date);
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/sell', $data);
		$this->load->view('customer_dashboard/common/footer');
	}

	public function sell() {

		// if ($date == NULL || $product == NULL || $time == NULL) {

		// 	$date = date('Y-m-d');
		// 	$data['customers'] = NULL;
		// } else {

		// 	$data['customers'] = $this->Common_Model->getCustomers($product, $time);
		// }

		$data['customers'] = $this->Common_Model->getCustomers();
		$data['products'] = $this->Common_Model->getProducts();
		
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/doSell', $data);
		$this->load->view('customer_dashboard/common/footer');
		$this->load->view('customer_dashboard/common/sellJS');
		
	}

	public function naga() {

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'n_customer', 'Naga Customer',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'n_product', 'Naga Product',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);
		
		$this->form_validation->set_rules(
				'n_date', 'Naga Date',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'n_limit', 'Naga Qty',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,4})?$/]',
						'greater_than[0]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid quantity',
						'greater_than' => '&#42;Invalid quantity'
				)
		);

		$this->form_validation->set_rules(
				'n_price', 'Naga Price',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid price'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$sendData = array(
					'error'   => 'Invalid input',
					'success' => FALSE
			);
			echo json_encode($sendData);

		} else {

			$data = array(
                'sl_customer' => $this->input->post('n_customer', TRUE),
                'sl_product'  => $this->input->post('n_product', TRUE),
                'sl_is_naga'  => 1,
                'sl_date'	  => $this->input->post('n_date', TRUE),
                'sl_qty'	  => $this->input->post('n_limit', TRUE),
                'sl_price'	  => $this->input->post('n_price', TRUE),
                'sl_public_id' => md5($this->input->post('n_customer', TRUE).$this->input->post('n_date', TRUE).time())
            );

            $result = $this->Sell_Model->naga($data);
            
			if ( $result ) {

				$sendData = $result;
				echo json_encode($sendData);

			} else {

				$sendData = array(
					'error'   => 'server error',
					'success' => FALSE
				);
				echo json_encode($sendData);
			}
		}
	}

	public function extra() {

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'x_customer', 'Extra Customer',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);
		
		$this->form_validation->set_rules(
				'x_date', 'Extra Date',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'x_pdt', 'Extra Product',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'x_qty', 'Extra Qty',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,4})?$/]',
						'greater_than[0]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid quantity',
						'greater_than' => '&#42;Invalid quantity'
				)
		);

		$this->form_validation->set_rules(
				'x_price', 'Extra Price',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid price'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$sendData = array(
					'error'   => $this->form_validation->error_array(),
					'success' => FALSE
			);
			echo json_encode($sendData);

		} else {

			$data = array(
                'sl_customer'  => strtolower($this->input->post('x_customer', TRUE)),
                'sl_product'   => strtolower($this->input->post('x_pdt', TRUE)),
                'sl_date'   => $this->input->post('x_date', TRUE),
                'sl_qty'      => $this->input->post('x_qty', TRUE),
                'sl_price'      => $this->input->post('x_price', TRUE),
                'sl_public_id' => md5($this->input->post('x_customer', TRUE).$this->input->post('x_date', TRUE).time())
            );

            $result = $this->Sell_Model->extra($data);
            
			if ( $result ) {

				$sendData = $result;
				echo json_encode($sendData);

			} else {

				$sendData = array(
					'error'   => 'server error',
					'success' => FALSE
				);
				echo json_encode($sendData);
			}
		}
	}

	public function closeSell() {

		$data['products'] = $this->Common_Model->getProducts();
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/completeSell', $data);
		$this->load->view('customer_dashboard/common/footer');
		$this->load->view('customer_dashboard/common/completeSellJS');
	}

	public function completeSell($date = NULL) {

		if ( !isset($date) ) {
			$date = date('Y-m-d');
		}

		$result = $this->Sell_Model->closeSell($date);
		
		if (!$result) {
			
			$this->session->set_flashdata('error', 'Serve error occured');
		}

		redirect('sell/closeSell');
	}

}