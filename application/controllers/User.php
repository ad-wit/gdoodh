<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {

		parent::__construct();
	}

	public function login() {

		$this->load->view('customer_dashboard/userLogin');
	}

	public function logout() {

		$this->load->view('customer_dashboard/userLogin');
	}
}