<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model('Customers_Model');
	}

	public function index() {

		$data['customers'] = $this->Common_Model->getCustomers();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/customers.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');
		
	}

	public function details($customer) {

		$data['customer'] = $this->Customers_Model->getCustomer($customer);
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/customerDetails.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');

	}

	public function add() {

		$data['products'] = $this->Common_Model->getProducts();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/addCustomerForm.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');		
	}

	public function addCustomer() {

		html_escape($_POST);

		$this->form_validation->set_rules(
				'customer-name', 'Customer\'s Name',
				array(
						'required',
						'trim'
						// array( 'is_customer_unique', array($this->Common_Model, 'is_customer_unique') )
				),
				array(
						'required' => '&#42;Please fill this field'
						// 'is_customer_unique' => '&#42;Product already exists'
				)
		);

		$this->form_validation->set_rules(
				'customer-contact', 'Customer\'s Contact',
				array(
						'required',
						'trim',
						'regex_match[/^[0-9_+]+$/]',
						'min_length[10]',
						'is_unique[customer.c_contact]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Please enter a valid %s',
						'min_length'  => '&#42;Please enter a valid %s',
						'is_unique'   => '&#42;Customer with same contact exists'
				)
		);

		$this->form_validation->set_rules(
				'customer-address', 'Customer\'s Address',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'customer-start', 'Customer\'s Start Date',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'customer-time', 'Customer\'s Time',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'customer-product', 'Customer\'s Products',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please select a product'
				)
		);

		$this->form_validation->set_rules(
				'customer-limit', 'Customer\'s Daily Limit',
				array(
						'required',
						'trim',
						'numeric'
				),
				array(
						'required' => '&#42;Please fill this field',
						'numeric' => '&#42;Please enter a valid value'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect('customers/add');

		} else {

			$data = array(
				'c_name'  => strtolower($this->input->post('customer-name', TRUE)),
				'c_address'  => strtolower($this->input->post('customer-address', TRUE)),
				'c_contact' => $this->input->post('customer-contact', TRUE),
				'c_start' => $this->input->post('customer-start', TRUE),
				'c_daily' => strtolower($this->input->post('customer-time', TRUE)),
				'c_product' => $this->input->post('customer-product', TRUE),
				'c_limit' => $this->input->post('customer-limit', TRUE),
				'c_public_id' => md5( $this->input->post('customer-name', TRUE).$this->input->post('customer-contact', TRUE).time() )
			);

			if ( $this->Customers_Model->add($data) ) {

				$this->session->set_flashdata('success', 'Customer added successfully');
				redirect('customers');
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect('customers/add');
			}
		}

	}

	public function edit( $customer = NULL ) {

		$data['products'] = $this->Common_Model->getProducts();
		$data['customer'] = $this->Customers_Model->getCustomer($customer);
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/editCustomerForm.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');		
	}

	public function editCustomer( $customer = NULL ) {

		html_escape($_POST);

		$this->form_validation->set_rules(
				'customer-name', 'Customer\'s Name',
				array(
						'required',
						'trim'
						// array( 'is_customer_unique', array($this->Common_Model, 'is_customer_unique') )
				),
				array(
						'required' => '&#42;Please fill this field'
						// 'is_customer_unique' => '&#42;Product already exists'
				)
		);

		$this->form_validation->set_rules(
				'customer-contact', 'Customer\'s Contact',
				array(
						'required',
						'trim',
						'regex_match[/^[0-9_+]+$/]',
						'min_length[10]',
						array( 'is_cst_unique', array($this->Common_Model, 'is_cst_unique') )
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Please enter a valid %s',
						'min_length'  => '&#42;Please enter a valid %s',
						'is_cst_unique'   => '&#42;User with same contact exists'
				)
		);

		$this->form_validation->set_rules(
				'customer-address', 'Customer\'s Address',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'customer-start', 'Customer\'s Start Date',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'customer-time', 'Customer\'s Time',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'customer-product', 'Customer\'s Products',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please select a product'
				)
		);

		$this->form_validation->set_rules(
				'customer-limit', 'Customer\'s Daily Limit',
				array(
						'required',
						'trim',
						'numeric'
				),
				array(
						'required' => '&#42;Please fill this field',
						'numeric' => '&#42;Please enter a valid value'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect("customers/edit/$customer");

		} else {

			$data = array(
				'c_name'  => strtolower($this->input->post('customer-name', TRUE)),
				'c_address'  => strtolower($this->input->post('customer-address', TRUE)),
				'c_contact' => $this->input->post('customer-contact', TRUE),
				'c_start' => $this->input->post('customer-start', TRUE),
				'c_daily' => strtolower($this->input->post('customer-time', TRUE)),
				'c_product' => $this->input->post('customer-product', TRUE),
				'c_limit' => $this->input->post('customer-limit', TRUE),
				'c_public_id' => $customer
			);

			if ( $this->Customers_Model->edit($data) ) {

				$this->session->set_flashdata('success', 'Customer updated successfully');
				redirect('customers');
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect("customers/edit/$customer");
			}
		}

	}

}