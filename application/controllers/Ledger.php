<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ledger extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model('Ledger_Model');
		$this->load->model('Suppliers_Model');
	}

	public function index() {

		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/ledger');
		$this->load->view('customer_dashboard/common/footer');
	}

	public function suppliers() {

		$data['suppliers'] = $this->Common_Model->getSuppliers();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/suppliersLedger.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');
	}

	public function customers() {

		$data['customers'] = $this->Common_Model->getCustomers();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/customersLedger.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');
	}

	public function customerLedger($customer = NULL, $month = NULL) {

		if( $customer == NULL ) {
			show_404();
			return;
		}

		if ( $month == NULL ) {
			$month = date('Y-m');
		}

		$data['ledger'] = $this->Ledger_Model->getCustomerLedger($customer, $month);

		if( $data['ledger']['customer'] == NULL ) {
			show_404();
			return;
		}

		$data['month'] = $month;

		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/customerLedgerView', $data);
		$this->load->view('customer_dashboard/common/footer');
		$this->load->view('customer_dashboard/common/customerLedgerJS');
	}

	public function supplierLedger($supplier = NULL) {

		if( $supplier == NULL ) {
			show_404();
			return;
		}

		$data['ledger'] = $this->Ledger_Model->getSupplierLedger($supplier);
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/supplierLedgerView', $data);
		$this->load->view('customer_dashboard/common/footer');
		$this->load->view('customer_dashboard/common/supplierLedgerJS');
	}

	public function pay( $supplier = NULL ) {

		if( $supplier == NULL ) {
			show_404();
			return;
		}

		$data['supplier'] = $this->Suppliers_Model->getSupplier($supplier);
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/paySupplier.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');
	}

	public function makePayment( $supplier = NULL ) {

		if( $supplier == NULL ) {
			show_404();
			return;
		}

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'payment-amt', 'Payment Amount',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]',
						'greater_than_equal_to[0]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid amount',
						'greater_than_equal_to' => '&#42;Invalid amount'
				)
		);

		$this->form_validation->set_rules(
				'payment-dues', 'Payment Dues',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid amount'
				)
		);

		$this->form_validation->set_rules(
				'payment-date', 'Payment Date',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect("ledger/suppliers/pay/$supplier");

		} else {

			$data = array(
				'py_supplier'  => $supplier,
				'py_amt' => $this->input->post('payment-amt', TRUE),
				'py_credit' => 1,
				'py_dues_left' => floatval($this->input->post('payment-dues', TRUE)) - floatval($this->input->post('payment-amt', TRUE)),
				'py_date' => $this->input->post('payment-date', TRUE),
				'py_public_id' => md5( $supplier.$this->input->post('payment-amt', TRUE).time() )
			);

			if ( $this->Ledger_Model->makePayment($data) ) {

				$this->session->set_flashdata('success', 'Payment done successfully!');
				redirect("ledger/suppliers/pay/$supplier");
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect("ledger/suppliers/pay/$supplier");
			}
		}

	}

}
