<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model('Purchases_Model');
	}

	public function index($date = NULL) {

		$data = $this->Common_Model->getProductsStock($date);
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/purchases', $data);
		$this->load->view('customer_dashboard/common/footer');
	}

	public function supplierPurchase( $supplier ) {

		$data['purchases'] = $this->Purchases_Model->getSupplierPurchase($supplier);
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/supplierPurchases', $data);
		$this->load->view('customer_dashboard/common/footer');		

	}

	public function getPurchase($id = NULL) {

		if( $id == NULL ) {

			$result = array(
				'error' => 'No such purchase is done',
				'success' => FALSE
			);
		} else {

			$result = $this->Purchases_Model->getPurchase($id);

			if (!$result) {

				$result = array(
					'error' => 'Some server error occured',
					'success' => FALSE
				);
			}
		}

		echo json_encode($result);
	}

	public function purchase() {

		$data['suppliers'] = $this->Common_Model->getSuppliers();
		$data['products'] = $this->Common_Model->getProducts();
		$this->load->view('customer_dashboard/common/head');
		$this->load->view('customer_dashboard/common/nav');
		$this->load->view('customer_dashboard/enterPurchases', $data);
		$this->load->view('customer_dashboard/common/footer');
		$this->load->view('customer_dashboard/common/purchaseJS');
	}

	public function enterPurchase() {

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'p_supplier', 'Purchase Supplier',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);
		
		$this->form_validation->set_rules(
				'p_product', 'Purchase Product',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'p_rate', 'Purchase Rate',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid rate'
				)
		);

		$this->form_validation->set_rules(
				'p_qty', 'Purchase Qty',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,4})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid quantity'
				)
		);

		$this->form_validation->set_rules(
				'p_amt', 'Purchase Amount',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid amount'
				)
		);

		$this->form_validation->set_rules(
				'p_date', 'Purchase Date',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'p_time', 'Purchase Time',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$sendData = array(
					'error'   => 'Invalid input',
					'success' => FALSE
			);
			echo json_encode($sendData);

		} else {

			$data = array(
                'prh_supplier'  => $this->input->post('p_supplier', TRUE),
                'prh_product'   => $this->input->post('p_product', TRUE),
                'prh_date'      => $this->input->post('p_date', TRUE),
                'prh_time'      => strtolower( $this->input->post('p_time', TRUE) ),
                'prh_rate'      => $this->input->post('p_rate', TRUE),
                'prh_qty'       => $this->input->post('p_qty', TRUE),
                'prh_amt'       => $this->input->post('p_amt', TRUE),
                'prh_public_id' => md5($this->input->post('p_supplier', TRUE).$this->input->post('p_amt', TRUE).time().rand())
            );
            
			if ( $this->Purchases_Model->enterPurchase($data) ) {

				$sendData = array(
					'error'   => FALSE,
					'success' => TRUE
				);
				echo json_encode($sendData);

			} else {

				$sendData = array(
					'error'   => 'server error',
					'success' => FALSE
				);
				echo json_encode($sendData);
			}
		}

	}

	public function addAmount() {

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'a_supplier', 'Supplier',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'a_date', 'Date',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'a_amount', 'Amount',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid rate'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$sendData = array(
					'error'   => 'Invalid input',
					'success' => FALSE
			);
			echo json_encode($sendData);

		} else {

			$data = array(
                'a_supplier'  => $this->input->post('a_supplier', TRUE),
                'a_date'      => $this->input->post('a_date', TRUE),
                'a_amt'       => $this->input->post('a_amount', TRUE),
                'a_public_id' => md5($this->input->post('a_supplier', TRUE).$this->input->post('a_amount', TRUE).time().rand())
            );
            
			if ( $this->Purchases_Model->addAmount($data) ) {

				$sendData = array(
					'error'   => FALSE,
					'success' => TRUE
				);
				echo json_encode($sendData);

			} else {

				$sendData = array(
					'error'   => 'server error',
					'success' => FALSE
				);
				echo json_encode($sendData);
			}
		}
	}

	public function paidAmount() {

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'pd_supplier', 'Supplier',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'pd_date', 'Date',
				array(
						'required',
						'trim'
				),
				array(
						'required'    => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'pd_amount', 'Amount',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid amount'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$sendData = array(
					'error'   => 'Invalid input',
					'success' => FALSE
			);
			echo json_encode($sendData);

		} else {

			$data = array(
                'pd_supplier'  => $this->input->post('pd_supplier', TRUE),
                'pd_date'      => $this->input->post('pd_date', TRUE),
                'pd_amt'       => $this->input->post('pd_amount', TRUE),
                'pd_public_id' => md5($this->input->post('pd_supplier', TRUE).$this->input->post('pd_amount', TRUE).time().rand())
            );
            
			if ( $this->Purchases_Model->paidAmount($data) ) {

				$sendData = array(
					'error'   => FALSE,
					'success' => TRUE
				);
				echo json_encode($sendData);

			} else {

				$sendData = array(
					'error'   => 'server error',
					'success' => FALSE
				);
				echo json_encode($sendData);
			}
		}
	}

}