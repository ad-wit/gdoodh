<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model('Products_Model');
	}

	public function index($date = NULL) {

		$data = $this->Common_Model->getProductsStock($date);
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/products.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');
		
	}

	public function add() {

		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/addProductForm.php');
		$this->load->view('customer_dashboard/common/footer.php');		
	}

	public function addProduct() {

		html_escape($_POST);

		$this->form_validation->set_rules(
		        'product-name', 'Product\'s Name',
		        array(
		        		'required',
		        		'trim',
		        		'is_unique[products.p_name]'
		        ),
		        array(
		                'required' => '&#42;Please fill this field',
		                'is_unique' => '&#42;Product already exists'
		        )
		);

		$this->form_validation->set_rules(
				'product-unit', 'Product\'s Unit',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'product-price', 'Product\'s Price',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,2})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid price'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect('products/add');

		} else {

			$data = array(
                'p_name'  => strtolower($this->input->post('product-name', TRUE)),
                'p_unit'  => strtolower($this->input->post('product-unit', TRUE)),
                'p_price' => $this->input->post('product-price', TRUE),
                'p_public_id' => md5($this->input->post('product-name', TRUE).time())
            );

			if ( $this->Products_Model->add($data) ) {

				$this->session->set_flashdata('success', 'Product added successfully');
				redirect('products');
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect('products/add');
			}
		}

	}

	public function edit( $product = NULL ) {

		$data['product'] = $this->Products_Model->getProduct($product);
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/editProductForm.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');		
	}

	public function editProduct( $product = NULL ) {

		html_escape($_POST);

		$this->form_validation->set_rules(
				'product-name', 'Product\'s Name',
				array(
						'required',
						'trim',
						array( 'is_pdt_unique', array($this->Common_Model, 'is_pdt_unique') )
				),
		        array(
		                'required'       => '&#42;Please fill this field',
		                'is_name_unique' => '&#42;Name already exists'
		        )
		);

		$this->form_validation->set_rules(
				'product-unit', 'Product\'s Unit',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'product-price', 'Product\'s Price',
				array(
						'required',
						'trim',
						'regex_match[/^\d{0,8}(\.\d{1,4})?$/]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Invalid price'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect("products/edit/$product");

		} else {

			$data = array(
                'p_name'      => strtolower($this->input->post('product-name', TRUE)),
                'p_unit'      => strtolower($this->input->post('product-unit', TRUE)),
                'p_price'     => $this->input->post('product-price', TRUE),
                'p_public_id' => $product
            );

			if ( $this->Products_Model->edit($data) ) {

				$this->session->set_flashdata('success', 'Product updated successfully');
				redirect('products');
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect("products/edit/$product");
			}
		}
	}

}