<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers extends CI_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model('Suppliers_Model');
	}

	public function index() {

		$data['suppliers'] = $this->Common_Model->getSuppliers();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/suppliers.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');
		
	}

	public function details($supplier) {

		$data['supplier'] = $this->Suppliers_Model->getSupplier($supplier);
		$data['products'] = $this->Common_Model->getProducts();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/supplierDetails.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');

	}

	public function add() {

		$data['products'] = $this->Common_Model->getProducts();
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/addSupplierForm.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');		
	}

	public function addSupplier() {

		html_escape($this->input->post());

		$this->form_validation->set_rules(
				'supplier-name', 'Supplier\'s Name',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'supplier-contact', 'Supplier\'s Contact',
				array(
						'required',
						'trim',
						'regex_match[/^[0-9_+]+$/]',
						'min_length[10]',
						'is_unique[supplier.s_contact]'
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Please enter a valid %s',
						'min_length'  => '&#42;Please enter a valid %s',
						'is_unique'   => '&#42;Supplier with same contact exists'
				)
		);

		$this->form_validation->set_rules(
				'supplier-address', 'Supplier\'s Address',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'supplier-start', 'Supplier\'s Start Date',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'supplier-product[]', 'Supplier\'s Products',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please select a product'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect('suppliers/add');

		} else {

			$data = array(
				's_name'  => strtolower($this->input->post('supplier-name', TRUE)),
				's_address'  => strtolower($this->input->post('supplier-address', TRUE)),
				's_contact' => $this->input->post('supplier-contact', TRUE),
				's_start' => $this->input->post('supplier-start', TRUE),
				's_product' => serialize( $this->input->post('supplier-product', TRUE) ),
				's_public_id' => md5( $this->input->post('supplier-name', TRUE).$this->input->post('supplier-contact'.time(), TRUE) )
			);

			if ( $this->Suppliers_Model->add($data) ) {

				$this->session->set_flashdata('success', 'Supplier added successfully');
				redirect('suppliers');
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect('suppliers/add');
			}
		}

	}

	public function edit( $supplier = NULL ) {

		$data['products'] = $this->Common_Model->getProducts();
		$data['supplier'] = $this->Suppliers_Model->getsupplier($supplier);
		$this->load->view('customer_dashboard/common/head.php');
		$this->load->view('customer_dashboard/common/nav.php');
		$this->load->view('customer_dashboard/editSupplierForm.php', $data);
		$this->load->view('customer_dashboard/common/footer.php');		
	}

	public function editSupplier( $supplier = NULL ) {

		html_escape($_POST);

		$this->form_validation->set_rules(
				'supplier-name', 'Supplier\'s Name',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'supplier-contact', 'Supplier\'s Contact',
				array(
						'required',
						'trim',
						'regex_match[/^[0-9_+]+$/]',
						'min_length[10]',
						array( 'is_sup_unique', array($this->Common_Model, 'is_sup_unique') )
				),
				array(
						'required'    => '&#42;Please fill this field',
						'regex_match' => '&#42;Please enter a valid %s',
						'min_length'  => '&#42;Please enter a valid %s',
						'is_sup_unique'   => '&#42;Supplier with same contact exists'
				)
		);

		$this->form_validation->set_rules(
				'supplier-address', 'Supplier\'s Address',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'supplier-start', 'Supplier\'s Start Date',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please fill this field'
				)
		);

		$this->form_validation->set_rules(
				'supplier-product[]', 'Supplier\'s Products',
				array(
						'required',
						'trim'
				),
				array(
						'required' => '&#42;Please select a product'
				)
		);

		if ($this->form_validation->run() == FALSE) {

			$this->session->set_flashdata('errors', $this->form_validation->error_array());
			$this->session->set_flashdata('values', $_POST);
			redirect("suppliers/edit/$supplier");

		} else {

			$data = array(
				's_name'  => strtolower($this->input->post('supplier-name', TRUE)),
				's_address'  => strtolower($this->input->post('supplier-address', TRUE)),
				's_contact' => $this->input->post('supplier-contact', TRUE),
				's_start' => $this->input->post('supplier-start', TRUE),
				's_product' => serialize($this->input->post('supplier-product', TRUE)),
				's_public_id' => $supplier
			);

			if ( $this->Suppliers_Model->edit($data) ) {

				$this->session->set_flashdata('success', 'Supplier updated successfully');
				redirect('suppliers');
			} else {

				$this->session->set_flashdata('error', 'Some server error occured please try again');
				redirect("suppliers/edit/$supplier");
			}
		}

	}

}