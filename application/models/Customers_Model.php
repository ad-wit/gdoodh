<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getCustomer( $customer = NULL ) {

		$this->db->join('products', 'customer.c_product = products.p_public_id', 'inner');
		$this->db->select('customer.c_name, customer.c_address, customer.c_contact, customer.c_start, customer.c_daily, customer.c_product, products.p_name, products.p_unit, customer.c_limit, customer.c_public_id');
		$customer = $this->db->get_where('customer', array('customer.c_public_id' => $customer), 1)->row_array();
		
		if ($customer) {

			return $customer;
		} else {
			
			return FALSE;
		}
	}

	public function add($data) {

		if( $this->db->insert('customer',$data) ) {

			$this->db->select('p_daily');
			$pdt = $this->db->get_where('products', array('p_public_id' => $data['c_product']), 1)->row_array();
			$pdt = floatval($pdt['p_daily']) + floatval($data['c_limit']);
			$this->db->update( 'products', array('p_daily' => $pdt), array('p_public_id' => $data['c_product']) );
			return TRUE;
		} else {

			return FALSE;
		}
	}

	public function edit($data) {

		$where = array('c_public_id' => $data['c_public_id']);
		if ($this->db->update('customer', $data, $where)) {
			
			return TRUE;
		} else {

			return FALSE;
		}
	}	

}