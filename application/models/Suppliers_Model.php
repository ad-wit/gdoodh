<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getSupplier( $supplier = NULL ) {

		$this->db->select('s_name, s_address, s_contact, s_start, s_product, s_dues, s_public_id');
		$supplier = $this->db->get_where('supplier', array('s_public_id' => $supplier), 1)->row_array();
		
		if ($supplier) {

			$supplier['s_product'] = unserialize($supplier['s_product']);
			return $supplier;
		} else {
			
			return FALSE;
		}
	}

	public function add($data) {

		if( $this->db->insert('supplier',$data) ) {

			return TRUE;
		} else {

			return FALSE;
		}
	}

	public function edit($data) {

		$where = array('s_public_id' => $data['s_public_id']);
		if ($this->db->update('supplier', $data, $where)) {
			
			return TRUE;
		} else {

			return FALSE;
		}
	}

}