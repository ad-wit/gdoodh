<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getProduct( $product = NULL ) {

		$this->db->select('p_name, p_unit, p_price, p_daily, p_public_id');
		$product = $this->db->get_where('products', array('p_public_id' => $product), 1)->row_array();
		if ($product) {

			return $product;
		} else {
			
			return FALSE;
		}
	}

	public function add($data) {

		if( $this->db->insert('products',$data) ) {
			
			return TRUE;
		} else {

			return FALSE;
		}
	}

	public function edit($data) {

		$where = array('p_public_id' => $data['p_public_id']);
		if ($this->db->update('products', $data, $where)) {
			
			return TRUE;
		} else {

			return FALSE;
		}
	}
}