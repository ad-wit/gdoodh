<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function validateDate($date, $format = 'm/d/Y') {

		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public function is_pdt_unique($value) {

		$values = $this->db->get_where( 'products', array('p_name' => $value) )->result_array();

		if ( count($values) > 1 ) {

			return FALSE;
		} else {
			
			return TRUE;
		}
	}

	public function is_cst_unique($value) {

		$values = $this->db->get_where( 'customer', array('c_contact' => $value) )->result_array();

		if ( count($values) > 1 ) {

			return FALSE;
		} else {
			
			return TRUE;
		}
	}

	public function is_sup_unique($value) {

		$values = $this->db->get_where( 'supplier', array('s_contact' => $value) )->result_array();

		if ( count($values) > 1 ) {

			return FALSE;
		} else {
			
			return TRUE;
		}
	}

	public function getProducts() {

		$this->db->select('p_name, p_unit, p_price, p_public_id');
		$this->db->order_by('p_name', 'asc');
		$products = $this->db->get('products')->result_array();
		foreach ($products as $key => $product) {
			$products[$product['p_public_id']] = $product;
			unset($products[$key]);
		}
		if ($products) {

			return $products;
		} else {
			
			return FALSE;
		}
	}

	public function getProductsStock($date = NULL) {

		if ($date == NULL) {
			$date = date('Y-m-d');
		}

		$this->db->select('prh_product, SUM(prh_qty) as stock');
		$this->db->group_by('prh_product');
		$stock_ary = $this->db->get_where('purchase', array('prh_date' => $date))->result_array();
		$stock = array();
		foreach ($stock_ary as $key => $pdt) {

			$stock[$pdt['prh_product']] = $pdt['stock'];
		}

		$this->db->select('p_name, p_unit, p_price, p_daily, p_public_id');
		$this->db->order_by('p_name', 'asc');
		$products = $this->db->get('products')->result_array();

		foreach ($products as $key => $pdt) {
			if (array_key_exists($pdt['p_public_id'], $stock)) {

				$pdt['stock'] = $stock[$pdt['p_public_id']];
				$pdt['in_cash'] = $stock[$pdt['p_public_id']] - $pdt['p_daily'];
			} else {

				$pdt['stock'] = 0;
				$pdt['in_cash'] = 0;
			}

			$products[$pdt['p_public_id']] = $pdt;
			unset($products[$key]);
		}

		$this->db->join('sell', 'customer.c_public_id = sell.sl_customer', 'inner');
		$this->db->select('sl_product, c_limit, sl_qty, sl_is_naga');
		$sell_ary = $this->db->get_where('customer', array('sl_date' => $date))->result_array();
		
		foreach ($sell_ary as $key => $sell) {
			$products[$sell['sl_product']]['in_cash'] = $products[$sell['sl_product']]['in_cash'] + $sell['c_limit'];
			if (!$sell['sl_is_naga']) {
				$products[$sell['sl_product']]['in_cash'] = $products[$sell['sl_product']]['in_cash'] - $sell['sl_qty'];
			}
		}

		$data['date'] = $date;
		$data['products'] = $products;

		return $data;
	}

	public function getCustomers() {

		$this->db->join('products', 'customer.c_product = products.p_public_id', 'inner');
		$this->db->select('c_name, c_contact, c_start, c_daily, c_product, p_name, p_unit, p_price, c_limit, c_public_id');

		$customers = $this->db->get('customer')->result_array();

		// if ($product && $time) {

		// 	$customers = $this->db->get_where('customer', array('c_product' => $product, 'c_daily' => $time))->result_array();
		// } else {

		// 	$customers = $this->db->get('customer')->result_array();
		// }

		if ($customers) {

			return $customers;
		} else {

			return FALSE;
		}
	}

	public function getSuppliers() {

		$this->db->select('s_name, s_contact, s_start, s_product, s_dues, s_public_id');
		$suppliers = $this->db->get('supplier')->result_array();
		foreach ($suppliers as $key => $supplier) {
			$supplier['s_product'] = unserialize($supplier['s_product']);
			$suppliers[$key] = $supplier;
		}

		if ($suppliers) {

			return $suppliers;
		} else {
			
			return FALSE;
		}
	}

}