<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sell_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getExtra($date) {

		$this->db->select('c_product, c_limit');
		$this->db->join('customer', 'sell.sl_customer = customer.c_public_id');
		$this->db->group_by(array('sl_customer'));
		$extra_naga_ary = $this->db->get_where('sell', array('sl_date' => $date, 'sl_is_naga' => 0))->result_array();
		$extra_naga = array();
		foreach ($extra_naga_ary as $x) {
			$extra_naga[$x['c_product']] = $x['c_limit'];
		}

		print_r($extra_naga);
		die();
	}	

	public function naga($data) {

		$sell = $this->db->get_where('sell', array('sl_date' => $data['sl_date'], 'sl_customer' => $data['sl_customer']), 1)->row_array();

		if ( $sell ) {

			if ($sell['sl_is_naga']) {

				$result = array(
					'error'   => 'Naga already done'
				);
			} else {

				$result = array(
					'error' => 'Extra entry done'
				);
			}

			$result['success'] = FALSE;
		} else {

			if( $this->db->insert('sell',$data) ) {
				
				$result = array(
					'error'   => FALSE,
					'success' => TRUE
				);
			} else {

				$result = array(
					'error'   => 'Serve error',
					'success' => FALSE
				);
			}

		}

		return $result;

	}

	public function extra($data) {

		if ( $this->db->get_where('sell', array('sl_date' => $data['sl_date'], 'sl_customer' => $data['sl_customer']))->result_array() ) {
			
			$result = array(
				'error'   => 'Entry already done',
				'success' => FALSE
			);
		} else {

			if( $this->db->insert('sell',$data) ) {
				
				$result = array(
					'error'   => FALSE,
					'success' => TRUE
				);
			} else {

				$result = array(
					'error'   => 'Serve error',
					'success' => FALSE
				);
			}
		}


		return $result;
	}

	public function closeSell($date) {
		
		if ( !$this->db->get_where('complete_sell', array('cs_date' => $date), 1)->row_array() ) {

			$this->db->select('p_instock, p_daily, p_naga, p_public_id');
			$this->db->order_by('p_name', 'asc');
			$products = $this->db->get('products')->result_array();

			if ($products != NULL) {
				foreach ($products as $key => $product) {
					$product['p_instock'] = floatval($product['p_instock']) - floatval($product['p_daily']) - floatval($product['p_naga']);
					$product['p_naga'] = 0;
					$this->db->where('p_public_id', $product['p_public_id']);
					$this->db->update('products', $product);
				}
			}

			$data = array(
						'cs_date' => $date,
						'cs_public_id' => md5(time().mt_rand())
						);

			if ($this->db->insert('complete_sell', $data)) {
				
				$this->session->set_flashdata('success', 'Stock Updated');
				return TRUE;

			} else {

				$this->session->set_flashdata('error', 'Serve error occured');
				return FALSE;
			}
		} else {

			$this->session->set_flashdata('error', "Sell already closed for $date");
			return TRUE;
		}
	}

}