<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ledger_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function makePayment($data) {

		if( $this->db->insert('payments',$data) ) {

			$this->db->select('s_dues');
			$dues = $this->db->get_where('supplier', array('s_public_id' => $data['py_supplier']), 1)->row_array();
			$dues = $dues['s_dues'];
			$dues = floatval($dues) - floatval($data['py_amt']);
			$this->db->update( 'supplier', array('s_dues' => $dues), array('s_public_id' => $data['py_supplier']) );
			return TRUE;
		} else {

			return FALSE;
		}
	}

	public function getSupplierLedger( $supplier ) {

		$this->db->join('supplier', 'supplier.s_public_id = payments.py_supplier');
		$this->db->from('payments');
		$this->db->where('py_supplier', $supplier);
		$this->db->order_by('py_date', 'asc');
		$ledger = $this->db->get()->result_array();
		if ($ledger) {

			return $ledger;
		} else {

			return FALSE;
		}
	}

	public function getCustomerLedger( $customer, $month ) {

		$this->db->join('products', 'products.p_public_id = customer.c_product');
		$this->db->select('c_name, c_contact, c_start, c_product, p_name, p_unit, p_price, c_daily, c_limit, c_public_id');
		$customer = $this->db->get_where('customer', array('c_public_id' => $customer), 1)->row_array();

		if ( $customer ) {

			$this->db->select('sl_date, sl_is_naga, p_name, p_unit, sl_qty, sl_price');
			$this->db->join('products', 'sell.sl_product = products.p_public_id');
			$this->db->like('sl_date', $month, 'after');
			$sell = $this->db->get_where('sell', array('sl_customer' => $customer['c_public_id']))->result_array();
			$data = array('customer' => $customer, 'sell' => $sell);
			
			return $data;

		} else {

			return FALSE;
		}
	}

}