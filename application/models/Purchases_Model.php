<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases_Model extends CI_Model {

	public function __construct() {

		parent::__construct();
	}

	public function getPurchase($id) {

		$this->db->join('products', 'products.p_public_id=purchase.prh_product', 'inner');
		$this->db->join('supplier', 'supplier.s_public_id=purchase.prh_supplier', 'inner');
		$this->db->select('purchase.prh_supplier, supplier.s_name, purchase.prh_product, products.p_name, products.p_unit, purchase.prh_date, purchase.prh_time, purchase.prh_rate, purchase.prh_qty, purchase.prh_amt, purchase.prh_public_id');
		$purchase = $this->db->get_where('purchase', array('prh_public_id' => $id), 1)->row_array();

		if ($purchase) {
			return array('error' => FALSE, 'success' => $purchase);
		} else {
			return array('error' => 'No such purchase done', 'success' => TRUE);
		}
	}

	public function getPurchases() {
		$this->db->join('products', 'products.p_public_id=purchase.prh_product', 'inner');
		$this->db->join('supplier', 'supplier.s_public_id=purchase.prh_supplier', 'inner');
		$this->db->select('purchase.prh_supplier, supplier.s_name, purchase.prh_product, products.p_name, products.p_unit, purchase.prh_date, purchase.prh_time, purchase.prh_rate, purchase.prh_qty, purchase.prh_amt, purchase.prh_public_id');
		$purchases = $this->db->get('purchase')->result_array();
		
		if ($purchases) {

			return $purchases;
		} else {
			
			return FALSE;
		}
		
	}

	public function getSupplierPurchase( $supplier ) {

		$this->db->join('products', 'products.p_public_id=purchase.prh_product', 'inner');
		$this->db->join('supplier', 'supplier.s_public_id=purchase.prh_supplier', 'inner');
		$this->db->select('purchase.prh_supplier, supplier.s_name, purchase.prh_product, products.p_name, products.p_unit, purchase.prh_date, purchase.prh_time, purchase.prh_rate, purchase.prh_qty, purchase.prh_amt, purchase.prh_public_id');
		$purchases = $this->db->get_where('purchase', array('prh_supplier' => $supplier))->result_array();

		if ($purchases) {

			return $purchases;
		} else {

			return FALSE;
		}
		
	}

	public function enterPurchase($data) {

		if( $this->db->insert('purchase',$data) ) {

			return TRUE;
		} else {

			return FALSE;
		}
	}

	public function addAmount($data) {

		$this->db->select('s_dues');
		$dues_ary = $this->db->get_where('supplier', array('s_public_id' => $data['a_supplier']), 1)->row_array();
		$dues = $dues_ary['s_dues'];
		$dues = floatval($dues) + floatval($data['a_amt']);
		$this->db->update( 'supplier', array('s_dues' => $dues), array('s_public_id' => $data['a_supplier']) );
		$ledger = array('py_supplier' => $data['a_supplier'],
						'py_amt'	  => $data['a_amt'],
						'py_dues_left'=> $dues,
						'py_date'	  => $data['a_date'],
						'py_public_id'=> $data['a_public_id']
						);

		if( $this->db->insert('payments', $ledger) ) {
					
			return TRUE;
		} else {

			return FALSE;
		}
	}

	public function paidAmount($data) {

		$this->db->select('py_dues_left');
		$this->db->order_by('py_id', 'desc');
		$dues = $this->db->get_where('payments', array('py_supplier' => $data['pd_supplier']), 1)->row_array();
		$dues = $dues['py_dues_left'];
		$dues = floatval($dues) - floatval($data['pd_amt']);
		$entry = array(
				'py_supplier'  => $data['pd_supplier'],
				'py_credit'    => 1,
				'py_amt'	   => $data['pd_amt'],
				'py_dues_left' => $dues,
				'py_date'	   => $data['pd_date'],
				'py_public_id' => $data['pd_public_id']
		);

		if( $this->db->insert('payments',$entry) ) {

			$this->db->update( 'supplier', array('s_dues' => $dues), array('s_public_id' => $data['pd_supplier']) );
			return TRUE;
		} else {

			return FALSE;
		}
	}

}